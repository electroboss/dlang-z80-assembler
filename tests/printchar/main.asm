printchar:
  ; put character location in de
  ld d, $70 ; chars at 0x7000
  ld b, 3
  sla b ; *8
  ld e,a

  ; TODO: prepare output for writing
  ; retrieve sprite data
  ld a,8
  push af
.vloop:
  ld c,(dei)
  ld b,8
  ld a,c
  jr .hloopstart
.hloop:
  ; src
  ld a,c
  ld c,1
  sra c
  ld c,a
.hloopstart:
  and %00000001
  jr z, .zero
  ld a, $ff
  out (1), a ; send a to output 1
  djnz .hloop ; dec b, jnz .hloop
  jp .hend
.zero:
  ld a, $00
  out (1), a ; send a to output 1
  djnz .hloop ; dec b, jnz .hloop
.hend:
  ; tell output is moved -8,1
  pop af
  dec a
  push af
  jr nz, .vloop
  ret