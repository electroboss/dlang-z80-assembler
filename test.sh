cd tests
TESTDIR=`pwd`
for test in *
do echo $test
cd $test
z80asm -o ../../testcorrect/$test.bin main.asm
../../main main.asm ../../testouts/$test.bin
cd "$TESTDIR"
cmp ../testouts/$test.bin ../testcorrect/$test.bin
read -p "Press Enter to continue" </dev/tty
done
