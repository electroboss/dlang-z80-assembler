import std.stdio : writeln;
import std.string : split, splitLines, StringException;
import std.array : join, replace;
import std.algorithm : countUntil, canFind;
import std.math.rounding : floor;
import std.conv : to;


struct AssembledSection {
  string label;
  ubyte[] assembledbytes;
  uint[] labellocs; // assembledbytes[labellocs] = location of label[0]; assembledbytes[labellocs+1] = location of label[1]
  string[] labels;
  ulong pos;
}

ubyte[] assemble(const string code) {
  const string[string] sections = code.splitSections;
  AssembledSection[] assembledSections;
  foreach (sectionkey; sections.keys) {
    string section = sections[sectionkey];
    AssembledSection assembledSection = assembleSection(section);
    assembledSection.label = sectionkey;
    assembledSections ~= assembledSection;
  }

  ulong[string] labellocs;
  string[] labels = [];
  ulong loc = 0;
  foreach (assembledSection; assembledSections) {
    labels ~= assembledSection.label;
    labellocs[assembledSection.label] = loc;
    loc += assembledSection.assembledbytes.length;
  }

  ubyte[] assembledbytes = [];
  foreach (assembledSection; assembledSections) {
    for (ulong i; i<assembledSection.labels.length; i++) {
      assembledSection.assembledbytes[assembledSection.labellocs[i]] =
      cast (ubyte) floor(cast(float) labellocs[assembledSection.labels[i]]/256);
      assembledSection.assembledbytes[assembledSection.labellocs[i]+1] =
      cast (ubyte) labellocs[assembledSection.labels[i]]%256;
    }
    if (assembledSection.pos) {
      if (assembledSection.pos < assembledbytes.length) {
        throw new Exception("Pos of section "~assembledSection.label~" already used.");
      } else {
        for (ulong i=0; i<assembledSection.pos-assembledbytes.length; i++) {
          assembledbytes ~= [0];
        }
      }
    }
    assembledbytes ~= assembledSection.assembledbytes;
  }

  return assembledbytes;
}

string[string] splitSections(const string str) {
  string lines;
  string[string] labels;
  string label = "start";
  foreach (line; str.splitLines) {
    if (line.split.length == 0) {
      continue;
    }
    line = line.split(";")[0];
    // make line replace spaces with one space
    line = line.split.join(" ");
    line = line.replace(", ",",");
    if (line.canFind(":")) {
      labels[label] = lines;
      lines = "";
      string a = line.split(":")[0];
      if (a[0] == '.') {
        label = label.split(".")[0] ~ a;
      } else {
        label = a;
      }
    }
    if ((line.split != []) & !line.canFind(":")){
      lines ~= line ~ "\n";
    }
  }
  labels[label] = lines;
  return labels;
}

AssembledSection assembleSection(const string section) {
  ubyte[] assembledbytes;
  foreach(line;section.splitLines) {
    assembledbytes ~= assembleOpcode(line);
  }
  AssembledSection output;
  output.assembledbytes = assembledbytes;
  output.labellocs = [];
  output.labels = [];
  return output;
}

ubyte[] assembleOpcode(const string opcode) {
  // Just ld opcodes
  if (opcode.split[0] == "ld"){
    string[] params = opcode.split[1..$].join.split(",");
    if (regorder.canFind(params[0]) & regorder.canFind(params[1])) {
      if ((params[0] == "(hl)") & (params[1] == "(hl)")) {
        throw new Exception("How???");
      }
      return [cast(ubyte) (0x40+8*regorder.countUntil(params[0])+regorder.countUntil(params[1]))];
    }
    // ld params[0], params[1]
    if ((params[0] == "(bc)") & (params[1] == "a")) {
      return [0x02];
    }
    if ((params[0] == "(de)") & (params[1] == "a")) {
      return [0x12];
    }
    if ((params[0] == "a") & (params[1] == "(bc)")) {
      return [0x0A];
    }
    if ((params[0] == "a") & (params[1] == "(de)")) {
      return [0x1A];
    }
    if ((params[0] == "sp") & (params[1] == "hl")) {
      return [0xF9];
    }
    if (params[0] == "bc") { // ld bc,nn
      return [ubyte(0x01)] ~ parseNum(params[1], 2);
    }
    if (params[0] == "de") { // ld de,nn
      return [ubyte(0x11)] ~ parseNum(params[1], 2);
    }
    if (params[0] == "hl") { // ld hl,nn
      if (params[1][0] == '(') { // ld hl,(nn)
        return [ubyte(0x2A)] ~ parseNum(params[1][1..$-1], 2); // 1..$-1 to get rid of surrounding ()
      }
      return [ubyte(0x21)] ~ parseNum(params[1], 2);
    }
    if (params[0] == "sp") {
      return [ubyte(0x31)] ~ parseNum(params[1], 2);
    }
    // ld hl,(nn) and ld a,(nn)
    if ((params[0] == "hl") & (params[1][0] == '(')) {
      return [ubyte(0x2A)] ~ parseNum(params[1][1..$-1], 2);
    }
    if ((params[0] == "a") & (params[1][0] == '(')) {
      return [ubyte(0x3A)] ~ parseNum(params[1][1..$-1], 2);
    }
    // ld r, n
    if (regorder.canFind(params[0])) {
      return [cast(ubyte) (8*regorder.countUntil(params[0])+6)] ~ parseNum(params[1],1);
    }
    // ld (nn), hl and ld (nn), a
    if ((params[1] == "hl") & (params[0][0] == '(')) {
      return [ubyte(0x22)] ~ parseNum(params[0][1..$-1], 2);
    }
    if ((params[1] == "a") & (params[0][0] == '(')) {
      return [ubyte(0x22)] ~ parseNum(params[0][1..$-1], 2);
    }

    writeln("Opcode, ", opcode, " invalid or not done yet.");
    return [0];
  }
  // hacky solution but idk what to do
  if (opcode.canFind("add a,")) {
    if (regorder.canFind(opcode.split(",")[1])) {
      return [cast(ubyte) (0x80 + regorder.countUntil(opcode.split(",")[1]))];
    }
  }
  if (opcode.canFind("adc a,")) {
    if (regorder.canFind(opcode.split(",")[1])) {
      return [cast(ubyte) (0x88 + regorder.countUntil(opcode.split(",")[1]))];
    }
  }
  if (opcode.canFind("sbc a,")) {
    if (regorder.canFind(opcode.split(",")[1])) {
      return [cast(ubyte) (0x98 + regorder.countUntil(opcode.split(",")[1]))];
    }
  }
  if (aluorder.canFind(opcode.split[0])) {
    if (regorder.canFind(opcode.split[1])) {
      return [cast(ubyte) (0x90 + 8*aluorder.countUntil(opcode.split[0])+regorder.countUntil(opcode.split[1]))];
    }
  }
  if (opcode.canFind("add hl,")) {
    string param = opcode.split(",")[1].split[0];
    switch (param) {
      case "bc":
        return [0x0A];
      case "de":
        return [0x1A];
      case "hl":
        return [0x2A];
      case "sp":
        return [0x3A];
      default:
        throw new Exception("Opcode: "~opcode~" Param invalid: " ~ param);
    }
  }
  if (opcode.canFind("inc")) {
    string param = opcode.split(" ")[1];
    if (!regorder.canFind(param)) {
      switch (param) {
        case "bc":
          return [0x03];
        case "de":
          return [0x13];
        case "hl":
          return [0x23];
        case "sp":
          return [0x33];
        default:
          throw new Exception("Opcode: "~opcode~" Param invalid: " ~ param);
      }
    }
    return [cast(ubyte) (4+8*regorder.countUntil(param))];
  }
  if (opcode.canFind("dec")) {
    string param = opcode.split[1];
    if (!regorder.canFind(param)) {
      switch (param) {
        case "bc":
          return [0x0B];
        case "de":
          return [0x1B];
        case "hl":
          return [0x2B];
        case "sp":
          return [0x3B];
        default:
          throw new Exception("Opcode: "~opcode~" Param invalid: " ~ param);
      }
    }
    return [cast(ubyte) (5+8*regorder.countUntil(param))];
  }
  if (opcode == "jp (hl)") {
    return [0xE9];
  }
  switch (opcode.split[0]) {
    case "nop":
      return [0];
    case "rlca":
      return [7];
    case "ex":
      switch (opcode.split[1]) {
        case "af,af'":
          return [0x08];
        case "de,hl":
          return [0xEB];
        case "(sp),hl":
          return [0xE3];
        default:
          break;
      }
      break;
    case "rrca":
      return [0x0F];
    case "rla":
      return [0x17];
    case "rra":
      return [0x1F];
    case "daa":
      return [0x27];
    case "cpl":
      return [0x2F];
    case "scf":
      return [0x37];
    case "ccf":
      return [0x3F];
    case "halt":
      return [0x76];
    case "ret":
      if (opcode.split.length == 1) {
        return [0xC9];
      }
      immutable string param = opcode.split[1];
      if (flagorder.canFind(param)) {
        return [cast(ubyte) (0xC0+0x08*flagorder.countUntil(param))];
      }
      break;
    case "pop":
      immutable string param = opcode.split[1];
      if (!wordregorder.canFind(param)) {
        break;
      }
      return [cast(ubyte) (0xC1+0x10*wordregorder.countUntil(param))];
    case "push":
      immutable string param = opcode.split[1];
      if (!wordregorder.canFind(param)) {
        break;
      }
      return [cast(ubyte) (0xC5+0x10*wordregorder.countUntil(param))];
    case "rst":
      switch (opcode.split[1]) {
        case "00h":
          return [0xC7];
        case "0h":
          return [0xC7];
        case "10h":
          return [0xD7];
        case "20h":
          return [0xE7];
        case "30h":
          return [0xF7];
        case "08h":
          return [0xCF];
        case "8h":
          return [0xCF];
        case "18h":
          return [0xDF];
        case "28h":
          return [0xEF];
        case "38h":
          return [0xFF];
        default:
          break;
      }
      break;
    case "exx":
      return [0xD9];
    case "ei":
      return [0xFB];
    case "di":
      return [0xF3];
    default:
      break;
  }

  writeln("Opcode ", opcode, " invalid or not done yet");
  return [0];
}

ubyte[] parseNum(string text, const ubyte len) {
  ubyte[] output;
  if (text[0] == '$') {
    text = text[1..$];
    if (text.length == 1) {
      output = [cast (ubyte) hexLetters.countUntil(text[0])];
      if (len == 2) {
        output = [ubyte(0)] ~ output;
      }
    } else if (text.length == 2) {
      output = [cast (ubyte) hexLetters.countUntil(text[1])];
      output[0] += cast (ubyte) hexLetters.countUntil(text[0])*0x10;
      if (len == 2) {
        output = [ubyte(0)] ~ output;
      }
    } else if (text.length == 4) {
      output = [cast (ubyte) hexLetters.countUntil(text[3])];
      output[0] += cast (ubyte) hexLetters.countUntil(text[2])*0x10;
      output ~= [cast (ubyte) hexLetters.countUntil(text[1])];
      output[1] += cast (ubyte) hexLetters.countUntil(text[0])*0x10;
      if (len == 1) {
        throw new Exception("Number $"~text~" too large");
      }
    } else {
      throw new Exception("Number $"~text~" invalid or I messed up");
    }
  } else if (text[0] == '%') {
    text = text[1..$];
    output ~= [0];
    for (ubyte i=0; i<8; i++) {
      output[0] += (2^i)*(text[$-(i+9)] == '1');
      if (!['0', '1'].canFind(text[$-(i+9)])) {
        throw new Exception("Number %"~text~" invalid.");
      }
    }
    if (len == 2) {
      output = [ubyte(0)] ~ output;
      for (ubyte i=0; i<8; i++) {
        output[0] += (2^i)*(text[$-(i+8)] == '1');
        if (!['0', '1'].canFind(text[$-(i+9)])) {
          throw new Exception("Number %"~text~" invalid.");
        }
      }
    }
  } else {
    int number = to!int(text);
    output ~= [cast(ubyte) number%256];
    output ~= [cast(ubyte) number/256];
  }
  return output;
}

const char[] hexLetters = [
  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
];

immutable string[] regorder = [
  "b","c","d","e","h","l","(hl)","a"
];

immutable string[] wordregorder = [
  "bc","de","hl","af"
];

immutable string[] aluorder = [
  "sub", "sbc a", "and", "xor", "or", "cp" // sbc wouldn't be used as it has a space
];

immutable string[] flagorder = [
  "nz","z","nc","c","po","pe","p","m"
];