import std;
import std.file : readText, write;
import std.array;
import std.string;
import std.stdio : writeln, writefln;
import std.algorithm;
import assemble : assemble;


int main(string[] args) {
  if (args.length <= 1) {
    writeln("Please specify an input file");
    return 1;
  }

  assert(isFile(args[1]));
  string outfile = "out.bin";
  if (args[2]) {
    outfile = args[2];
  }
  auto infile = args[1].readText;
  ubyte[] output = assemble(infile);
  
  write(outfile,output);

  return 0;
}